require('rootpath')();
const express = require('express');
const app = express();
const winston = require('./winston');
const bodyParser = require('body-parser');
var morgan = require('morgan');

app.use(bodyParser.json());

app.use(morgan('combined', { stream: winston.stream }));

// api routes
app.use('/babelfish', require('./babelfish.controller'));

// global error handler

// start server
const port = process.env.NODE_ENV === 'production' ? 80 : 5000;
const server = app.listen(port, function () {
    winston.info('Server listening on port ' + port);
});